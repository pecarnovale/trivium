﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trivium.Core
{
    public class FlipFlop
    {

        private bool hasRightNeighbor;
        private FlipFlop rightNeighbor;
        private byte value;

        public FlipFlop()
        {
            value = 0;
            hasRightNeighbor = false;

        }

        public FlipFlop(FlipFlop neighbor)
        {
            value = 0;
            hasRightNeighbor = true;
            this.rightNeighbor = neighbor;
        }

   
       public void tick(byte value)
       {
           //if this FlipFlop has a neighbor, then cause it to tick
           if (hasRightNeighbor)
           {
               rightNeighbor.tick(this.value);
           }

           this.value = value;
       }

        public FlipFlop getRightNeighbor()
        {
            return rightNeighbor;
        }

        public byte getValue()
        {
            return value;
        }
    }
}