﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trivium.Core
{
    public class Trivium
    {

        //this register contains bits 1-93 (0-92)
        private TriviumShiftRegister registerOne;
        //this register contains bits 94-177 (93-176) --> (0-83)
        private TriviumShiftRegister registerTwo;
        //this register conatins bits 178-288 (177-287) --> (0-110)
        private TriviumShiftRegister registerThree;

        public Trivium(byte[] key, byte[] IV, bool bigEndian)
        {
            registerOne = new TriviumShiftRegister(93);
            registerTwo = new TriviumShiftRegister(84);
            registerThree = new TriviumShiftRegister(111);

            if (bigEndian)
            {
                initializeRegistersBigEndian(key, IV);
            }
            else
            {
                initializeRegistersLittleEndian(key, IV);
            }

            initializationRounds();
        }

        //Devuelve un Keystream de 8 bits
        public byte getKeyByte()
        {
            byte result = 0x00;

            byte lowbits = 0x00;
            byte highbits = 0x00;

            for (int j = 0; j < 4; j++)
            {
                lowbits |= (byte)(this.getKeyBit() << j);
            }

            for (int j = 0; j < 4; j++)
            {
                highbits |= (byte)(this.getKeyBit() << j);
            }

            result = (byte)(highbits << 4 | lowbits);

            return result;
        }

        //GENERACION DE BIT PARA KEY STREAM -- PAGINA 578 -- Figura 2 Generación de llave para keystream
        public byte getKeyBit()
        {

            /*
             * Used from the Trivium specifications, can also be used for
             * initializing the registers
             *
             * t1    =   s66 + s93  
             * t2    =   s162 + s177
             * t3    =   s243 + s288
             *        
             * z     =   t1 + t2 + t3
             *        
             * t1    =   t1 + s91  s92 + s171
             * t2    =   t2 + s175  s176 + s264
             * t3    =   t3 + s286  s287 + s69
             * (s1; s2; : : : ; s93) --> (t3; s1; : : : ; s92)
             * (s94; s95; : : : ; s177) --> (t1; s94; : : : ; s176)
             * (s178; s279; : : : ; s288) --> (t2; s178; : : : ; s287)   
             */

            byte result = 0x00;

            byte[] regOneBits = registerOne.getBits(new[] { 65, 90, 91, 92, 68 });
            byte[] regTwoBits = registerTwo.getBits(new[] { 68, 81, 82, 83, 77 });
            byte[] regThreeBits = registerThree.getBits(new[] { 65, 108, 109, 110, 86 });

            byte t1, t2, t3;

            //z     =   t1 + t2 + t3
            result = (byte)(regOneBits[0] ^ regOneBits[3]
                    ^ regTwoBits[0] ^ regTwoBits[3]
                    ^ regThreeBits[0] ^ regThreeBits[3]);

            //t1    =   t1 + s91  s92 + s171
            t1 = (byte)(regOneBits[1] & regOneBits[2]);
            t1 ^= (byte)(regTwoBits[4] ^ regOneBits[0] ^ regOneBits[3]);

            // t2    =   t2 + s175  s176 + s264
            t2 = (byte)(regTwoBits[1] & regTwoBits[2]);
            t2 ^= (byte)(regThreeBits[4] ^ regTwoBits[0] ^ regTwoBits[3]);

            //t3    =   t3 + s286  s287 + s69
            t3 = (byte)(regThreeBits[1] & regThreeBits[2]);
            t3 ^= (byte)(regOneBits[4] ^ regThreeBits[0] ^ regThreeBits[3]);

            //(s1; s2; : : : ; s93) --> (t3; s1; : : : ; s92)
            registerOne.loadValue(t3);

            //(s94; s95; : : : ; s177) --> (t1; s94; : : : ; s176)
            registerTwo.loadValue(t1);

            //(s178; s279; : : : ; s288) --> (t2; s178; : : : ; s287)   
            registerThree.loadValue(t2);

            //Al finalizar cada iteración, se obtiene bit del keystream al cual se le aplica la operación lógica OR-Exclusiva para el cifrado del texto.
            return result;
        }


       
        private void initializeRegistersBigEndian(byte[] key, byte[] IV)
        {
            //INICIALIZACION DE KEY -- HOJA 578 -- PRIMEROS 80 BITS CON LA KEY EL RESTO EN CERO -- (ACA VA POR BYTE PERO SI SE VE CADA FLIPFLOP DEL REGISTRO UNO SE PUEDE VER LOS BITS)
            for (int i = key.Count() - 1; i >= 0; i--)
            {
                byte temp = 0x00;
                for (int j = 0; j < 8; j++)
                {
                    temp = (byte)(key[i] >> j);
                    temp = (byte)(temp & 1);
                    registerOne.loadValue(temp);
                }
            }

            //INICIALIZACION DE KEY -- HOJA 578 -- PRIMEROS 80 BITS CON EL IV EL RESTO EN CERO -- (ACA VA POR BYTE PERO SI SE VE CADA FLIPFLOP DEL REGISTRO UNO SE PUEDE VER LOS BITS) 
            for (int i = IV.Count() - 1; i >= 0; i--)
            {
                byte temp = 0x00;
                for (int j = 0; j < 8; j++)
                {
                    temp = (byte)(IV[i] >> j);
                    temp = (byte)(temp & 1);
                    registerTwo.loadValue(temp);
                }
            }

            //EL TERCER REGISTRO TIENE TODOS LOS BITS EN 0 MENOS LOS ULTIMOS 3
            registerThree.loadValue((byte)0x01);
            registerThree.loadValue((byte)0x01);
            registerThree.loadValue((byte)0x01);

            for (int i = 0; i < registerThree.size() - 3; i++)
            {
                registerThree.loadValue((byte)0x00);
            }

        }

        private void initializeRegistersLittleEndian(byte[] key, byte[] IV)
        {
            //init first register
            for (int i = 0; i < key.Count(); i++)
            {
                byte temp = 0x00;
                for (int j = 0; j < 8; j++)
                {
                    temp = (byte)(key[i] >> j);
                    temp = (byte)(temp & 1);
                    registerOne.loadValue(temp);
                    Console.Write(temp);
                }
                Console.Write(" ");
            }

            //init second register
            for (int i = 0; i < IV.Count(); i++)
            {
                byte temp = 0x00;
                for (int j = 0; j < 8; j++)
                {
                    temp = (byte)(IV[i] >> j);
                    temp = (byte)(temp & 1);
                    registerTwo.loadValue(temp);
                }
            }

            //init third register
            registerThree.loadValue((byte)0x01);
            registerThree.loadValue((byte)0x01);
            registerThree.loadValue((byte)0x01);

            for (int i = 0; i < registerThree.size() - 3; i++)
            {
                registerThree.loadValue((byte)0x00);
            }

        }

        private void initializationRounds()
        {
            for (int i = 0; i < (4 * 288); i++)
            {
                this.getKeyBit();
            }
        }
    }
}