﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trivium.Core
{
    interface ShiftRegister
    {
         int size();

         byte getOuput();

         byte getBitAt(int position);

         void loadValue(byte value);

         byte[] getBits(int [] positions);
    }
}