﻿using System;
using System.IO;

namespace Trivium.Core
{
    public class TextEncrypt
    {

        public static int MAX_READ_BUFFER_SIZE = 2048;

        Trivium cipher;
        FileStream reader;
        FileStream writer;

        public TextEncrypt(Trivium cipher)
        {
            this.cipher = cipher;
        }

        public TextEncrypt(String inputFilePath, String outputFilePath, Trivium cipher)
        {
            this.cipher = cipher;

            reader = new FileStream(inputFilePath,FileMode.Open);
            writer = new FileStream(outputFilePath, FileMode.Create);
        }

        public void encrypt()
        {
            int readBytes = 0;

            byte[] buffer = new byte[MAX_READ_BUFFER_SIZE];

            do
            {
                readBytes = reader.Read(buffer, 0, MAX_READ_BUFFER_SIZE);

                for (int i = 0; i < readBytes; i++)
                {
                    buffer[i] ^= cipher.getKeyByte();
                }

                if (readBytes > 0)
                {
                    writer.Write(buffer, 0, readBytes);
                }

            } while (readBytes > 0);

            reader.Close();
            writer.Close();
        }

        public byte[] encryptText(byte [] strIn)
        {
            int readBytes = 0;

            byte[] buffer = strIn;


            for (int i = 0; i < strIn.Length; i++)
            {
                buffer[i] ^= cipher.getKeyByte();
            }


            return buffer;
        }


    }
}