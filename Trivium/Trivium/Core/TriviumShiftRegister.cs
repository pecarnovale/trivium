﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Trivium.Core
{
    public class TriviumShiftRegister : ShiftRegister
    {


    private FlipFlop[] flipFlops;
    private byte tap;

    
    public TriviumShiftRegister(int sizeOfRegister)
    {
        flipFlops = new FlipFlop[sizeOfRegister];

        flipFlops[sizeOfRegister - 1] = new FlipFlop();

        for (int i = (flipFlops.Count() - 2); i >= 0; i--)
        {
            flipFlops[i] = new FlipFlop(flipFlops[i + 1]);
        }

        tap = 0x00;
    }

    public int size()
    {
        return this.flipFlops.Count();
    }

    
    public byte getOuput()
    {
        return this.tap;
    }

   
    public byte getBitAt(int position)
    {
        return flipFlops[position].getValue();
    }

   
    public void loadValue(byte value)
    {
        flipFlops[0].tick(value);
    }

  
    public byte[] getBits(int [] positions)
    {
        byte[] result = new byte[positions.Count()];

        for (int i = 0; i < result.Count(); i++)
        {
            result[i] = flipFlops[positions[i]].getValue();
        }

        return result;
    }
}

}