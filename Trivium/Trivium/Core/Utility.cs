﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Trivium.Core
{
    public static class Utility
    {

        public static byte[] stringToHex(string st)
        {
            return Encoding.ASCII.GetBytes(st).ToArray();
        }

        public static byte[] swapEndianness(byte[] bytes)
        {
            byte[] result = new byte[bytes.Count()];

            for (int i = 0; i < bytes.Count(); i++)
            {
                result[i] = bytes[bytes.Count() - 1 - i];
            }

            return result;
        }

        public static String byteToHexString(byte b)
        {
            String result = "";

            byte upperByte = (byte)((b & 0xf0) >> 4);

            result += byteToChar(upperByte);

            byte lowerByte = (byte)((b & 0x0f));

            result += byteToChar(lowerByte);

            return result;
        }

        private static char byteToChar(byte c)
        {
            char result = '0';

            if (c <= 9 && c >= 0)
            {
                result = (char)(c + '0');
            }

            if (c <= 15 && c >= 10)
            {
                result = (char)(c - 10 + 'A');
            }

            return result;
        }

        public static byte charToHex(char c)
        {
            byte result = 0x00;

            if (c <= '9' && c >= '0')
            {
                result = (byte)(c - '0');
            }

            if (c <= 'F' && c >= 'A')
            {
                result = (byte)(c - 'A' + 10);
            }

            if (c <= 'f' && c >= 'a')
            {
                result = (byte)(c - 'a' + 10);
            }

            return result;
        }
    }
}

