﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Trivium.Core;

namespace Trivium.Controllers
{
    [ValidateInput(false)]
    public class HomeController : Controller
    {

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GuardarConfiguracion(string submitButton)
        {

            if(submitButton == "Guardar")
            {
                if (Session["iv"] == null && Session["key"] == null)
                {
                    Session["iv"] = Request["iv"];
                    Session["key"] = Request["key"];
                    Session["configurado"] = true;
                }
            }
            
            if(submitButton == "Reconfigurar")
            {
                Session["iv"] = null;
                Session["key"] = null;
                Session["configurado"] = false;
            }

            return View("Index");
        }

        [HttpPost]
        public ActionResult ProcesarArchivo(string ivKeyBigEndian, string loadBigEndian)
        {

            if (Request.Files.Count > 0)
            {
                //Subir Archivo
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Files/"), fileName);
                    var outPath = Path.Combine(Server.MapPath("~/Files/"), "Out_" + fileName);
                    file.SaveAs(path);



                    //array to hold the key and iv
                    byte[] iv;
                    byte[] key;


                    bool keyAndIVBigEndian = !string.IsNullOrEmpty(ivKeyBigEndian);
                    bool dataWithBigEndian = !string.IsNullOrEmpty(loadBigEndian);

                    key = Utility.stringToHex(Session["key"].ToString());
                    iv = Utility.stringToHex(Session["iv"].ToString());

                    if (!keyAndIVBigEndian && dataWithBigEndian)
                    {
                        key = Utility.swapEndianness(key);
                        iv = Utility.swapEndianness(iv);
                    }

                    if (keyAndIVBigEndian && !dataWithBigEndian)
                    {
                        key = Utility.swapEndianness(key);
                        iv = Utility.swapEndianness(iv);
                    }

                    var cipher = new Trivium.Core.Trivium(key, iv, dataWithBigEndian);

                    var encryptor = new TextEncrypt(path, outPath, cipher);

                    encryptor.encrypt();

                    ViewBag.OutPath = "/Files/" + "Out_" + fileName;

                }

            }
            return View("Index");
        }

        [HttpPost]
        public ActionResult ProcesarTexto(string ivKeyBigEndian, string loadBigEndian)
        {

            //array to hold the key and iv
            byte[] iv;
            byte[] key;

            bool keyAndIVBigEndian = !string.IsNullOrEmpty(ivKeyBigEndian);
            bool dataWithBigEndian = !string.IsNullOrEmpty(loadBigEndian);

            key = Utility.stringToHex(Session["key"].ToString());
            iv = Utility.stringToHex(Session["iv"].ToString());

            if (!keyAndIVBigEndian && dataWithBigEndian)
            {
                key = Utility.swapEndianness(key);
                iv = Utility.swapEndianness(iv);
            }

            if (keyAndIVBigEndian && !dataWithBigEndian)
            {
                key = Utility.swapEndianness(key);
                iv = Utility.swapEndianness(iv);
            }

            var cipher = new Trivium.Core.Trivium(key, iv, dataWithBigEndian);

            var encryptor = new TextEncrypt(cipher);

            ViewBag.Out = Encoding.Default.GetString(encryptor.encryptText(Encoding.Default.GetBytes(Request["texto"])));

            return View("Index");
        }
    }
}